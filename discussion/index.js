// SECTION While Loop

// If the condition evaluates to true, the statements inside the code block will executed

/*
SYNTAX:

	While(expression/condition){
		statement
	}
*/

/*let count = 0;

while(count !== 5){
	console.log("While: " + count);

	// iteration - it increases the value of count after every iteration to stop the loop when it reaches 5.

	// increment (++) and decrement(--)
	count++;
}*/

// SECTION - Do While Loop

// A do-while loop works a lot like the while loop. But unline while loops, do-while loops guarantee that the code will be executedat least once.

/*
	SYNTAX:

	do {
		statement
	} while (expression/condition)
*/

let number = Number(prompt("Give me a number"));

do {

	console.log("Do While: " + number);

	number +=1;
} while(number < 10);



// SECTION - For Loops

// A for loop is more flexible than while and do-while loop
/*
	SYNTAX

	for(initialization;, expression/condition; finalExpression){
	statement
	}
*/


/*for (let count = 0; count <= 20; count++) {
	console.log(count);
}*/

// Characters in strings may be counted using the .length property.

let myString = prompt("Give me a word");

console.log(myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);

// Will create a loop that will print out the individual letters of a variable.

for (let x = 0; x < myString.length; x++){

	// The current value of my string is printed out using it's index value.
	console.log(myString[x])
}

// Changing of vowels using loops

// if the character of your name is a vowel letter, instead of displaying the character, display number 3
let myName = "DelIZo";

for (let i = 0; i < myName.length; i++){
	if (
			myName[i].toLowerCase() == "a" || 
			myName[i].toLowerCase() == "e" || 
			myName[i].toLowerCase() == "i" || 
			myName[i].toLowerCase() == "o" || 
			myName[i].toLowerCase() == "u" 
		) {
			console.log[3];
		} else {
			console.log(myName[i]);
		}
}