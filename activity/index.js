

let x = 100;

console.log("The given number is " + x);

for (x; x >= 50 ; x -=5){
	
	if (
		x == 100 ||
		x == 90 ||
		x == 80 ||
		x == 70 ||
		x == 60 
		) {

		console.log("The number is divisible by 10. Skipping the number");
} else if( 
	x == 95 ||
	x == 85 ||
	x == 75 ||
	x == 65 ||
	x == 55 
	) 
{
	console.log (x);
} else {
	console.log("The current value is at 50. Terminating the loop")
}
}






function removeVowels(word) {
	let consonant = "";
	let vowels = "";
	for( let i = 0; i < word.length; i++){

		if( 
			word[i] == "a" ||
			word[i] == "e" ||
			word[i] == "i" ||
			word[i] == "o" ||
			word[i] == "u" 
			){
			vowels += word[i];		
	} else
	consonant += word[i];
}
return consonant;
}

function removeConsonants(word) {
	let consonant = "";
	let vowels = "";
	for( let i = 0; i < word.length; i++){

		if( 
			word[i] == "a" ||
			word[i] == "e" ||
			word[i] == "i" ||
			word[i] == "o" ||
			word[i] == "u" 
			){
			vowels += word[i];		
	}
	consonant += word[i];
}
return vowels;
}


let consonantsOnly = removeVowels("supercalifragilisticexpialidocious");
let vowelsOnly = removeConsonants("supercalifragilisticexpialidocious");
console.log(consonantsOnly);
console.log(vowelsOnly);